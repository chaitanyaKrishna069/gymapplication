package com.epam.authorization.exception;

@SuppressWarnings("serial")
public class ProtectionException extends RuntimeException {
	public ProtectionException(String message) {
		super(message);
	}
}