package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.authorization.repository.UserRepository;
import com.epam.authorization.service.AuthenticationService;
import com.epam.authorization.service.JwtService;

@ExtendWith(MockitoExtension.class)
 class AuthServiceTest {

	@Mock
	private UserRepository repository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@Mock
	private JwtService jwtService;

	@InjectMocks
	private AuthenticationService authService;

	@BeforeEach
	@SuppressWarnings("deprecation")
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	 void testGenerateToken() {
		String username = "testUser";
		String mockToken = "mockToken";

		when(jwtService.generateToken(username)).thenReturn(mockToken);

		String generatedToken = authService.generateToken(username);

		assertEquals(mockToken, generatedToken);
		verify(jwtService, times(1)).generateToken(username);
		verifyNoMoreInteractions(jwtService);
	}
}