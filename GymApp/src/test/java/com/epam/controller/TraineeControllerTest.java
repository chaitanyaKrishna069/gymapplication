package com.epam.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.controller.TraineeController;
import com.epam.gymapp.dto.input.TraineeDTO;
import com.epam.gymapp.dto.input.TraineeRegistrationDTO;
import com.epam.gymapp.dto.input.UpdatedTraineersDTO;
import com.epam.gymapp.dto.output.DisplayTraineeDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.TrainersDTO;
import com.epam.gymapp.dto.output.UpdatedTraineeDTO;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.User;
import com.epam.gymapp.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = TraineeController.class)
class TraineeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TraineeService traineeService;

	@Test
	void addTraineeWithDtoTest() throws Exception {
		User user = new User(0, "rama", "haresh", "999@gmail.com", "123", true);
		Trainee trainee = new Trainee(0, new Date(), "vrindavan", user);
		TraineeRegistrationDTO traineeRegDto = new TraineeRegistrationDTO("rama", "haresh", "999@gmail.com",
				trainee.getDateOfBirth(), trainee.getAddress());

		Mockito.when(traineeService.addTrainee(traineeRegDto)).thenReturn(any(RegistrationResponseDTO.class));
		mockMvc.perform(
				post("/gymapp/trainees").contentType(MediaType.APPLICATION_JSON).content(asJsonString(traineeRegDto)))
				.andExpect(status().isCreated());
	}

	@Test
	void viewTraineeTest() throws Exception {

		Mockito.when(traineeService.getTraineeByUsername("murari@gmail.com")).thenReturn(any(DisplayTraineeDTO.class));
		mockMvc.perform(get("/gymapp/trainees/murari@gmail.com")).andExpect(status().isOk());
	}

	@Test
	void updateTraineeTest() throws Exception {
		TraineeDTO traineeDto = new TraineeDTO("murari@gmail.com", "gopala", "krishna", new Date(), "vellore", true);
		Mockito.when(traineeService.updateTrainee(any(TraineeDTO.class))).thenReturn(any(UpdatedTraineeDTO.class));
		mockMvc.perform(
				put("/gymapp/trainees").contentType(MediaType.APPLICATION_JSON).content(asJsonString(traineeDto)))
				.andExpect(status().isCreated());
	}

	@Test
	void deleteTraineeTest() throws Exception {
		Mockito.doNothing().when(traineeService).deleteTrainee("murari@gmail.com");
		mockMvc.perform(delete("/gymapp/trainees/murari@gmail.com")).andExpect(status().isOk());

	}

	@Test
	void updateTrainerListTest() throws Exception {
		List<String> list = new ArrayList<>();
		list.add("acharya@gmail.com");
		UpdatedTraineersDTO updateTraineeList = new UpdatedTraineersDTO("murari@gmail.com", list);
		mockMvc.perform(put("/gymapp/trainees/trainers-updates").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(updateTraineeList))).andExpect(status().isCreated());
	}

	@Test
	void notAssignedTrainerListTest() throws Exception {
		List<TrainersDTO> list = new ArrayList<>();
		list.add(TrainersDTO.builder().userName("acharya@gmail.com").firstName("n").lastName("k")
				.specializationName("zumba").build());
		Mockito.when(traineeService.unAssignedTrainers("murari@gmail.com")).thenReturn(list);
		mockMvc.perform(get("/gymapp/trainees/trainers-un-assigned/murari@gmail.com"))
				.andExpect(status().isAccepted());
	}

	private static String asJsonString(Object object) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}