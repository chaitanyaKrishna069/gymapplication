package com.epam.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.controller.TrainerController;
import com.epam.gymapp.dto.input.TrainerDTO;
import com.epam.gymapp.dto.input.TrainerRegistrationDTO;
import com.epam.gymapp.dto.output.DisplayTrainerDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.UpdatedTrainerDTO;
import com.epam.gymapp.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = TrainerController.class)
class TrainerControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TrainerService trainerService;

	@Test
	void addTrainerWithDtoTest() throws Exception {
		TrainerRegistrationDTO trainerRegistrationDTO = new TrainerRegistrationDTO("krishna", "gopala",
				"murari@gmail.com", 1);
		RegistrationResponseDTO responseDto = new RegistrationResponseDTO("murari@gmail.com", "krishna");
		Mockito.when(trainerService.addTrainer(trainerRegistrationDTO)).thenReturn(responseDto);
		mockMvc.perform(post("/gymapp/trainers").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainerRegistrationDTO))).andExpect(status().isCreated());
	}

	@Test
	void viewTraineeTest() throws Exception {
		Mockito.when(trainerService.getTrainerByUsername("acharya@gmail.com")).thenReturn(any(DisplayTrainerDTO.class));
		mockMvc.perform(get("/gymapp/trainers/acharya@gmail.com")).andExpect(status().isOk());
	}

	@Test
	void updateTraineeTest() throws Exception {
		TrainerDTO trainerDto = new TrainerDTO("murari@gmail.com", "gopala", "krishna", true, "zumba");
		Mockito.when(trainerService.updateTrainer(any(TrainerDTO.class))).thenReturn(any(UpdatedTrainerDTO.class));
		mockMvc.perform(
				put("/gymapp/trainers").contentType(MediaType.APPLICATION_JSON).content(asJsonString(trainerDto)))
				.andExpect(status().isCreated());
	}

	private static String asJsonString(Object object) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}