package com.epam.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.controller.TrainingController;
import com.epam.gymapp.dto.input.TrainingByDatesDTO;
import com.epam.gymapp.dto.input.TrainingDTO;
import com.epam.gymapp.dto.output.TraineeTrainingDTO;
import com.epam.gymapp.dto.output.TrainerTrainingDTO;
import com.epam.gymapp.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = TrainingController.class)
class TrainingControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TrainingService trainingService;

	@Test
	void addTrainingWithDtoTest() throws Exception {
		TrainingDTO training = new TrainingDTO();
		Mockito.when(trainingService.registerTraining(training)).thenReturn(training);
		mockMvc.perform(
				post("/gymapp/trainings").contentType(MediaType.APPLICATION_JSON).content(asJsonString(training)))
				.andExpect(status().isCreated());
	}

	@Test
	void viewTraineeTrainingListPeriodTest() throws Exception {
		TrainingByDatesDTO trainingBetweenDates = new TrainingByDatesDTO("ramesh@gmail.com", new Date(), new Date());
		List<TraineeTrainingDTO> list = new ArrayList<>();
		list.add(TraineeTrainingDTO.builder().trainerUsername("acharya@gmail.com").trainingDate(new Date())
				.trainingName("test").build());
		Mockito.when(trainingService.displayTraineeTrainingsByDuration(trainingBetweenDates)).thenReturn(list);
		mockMvc.perform(post("/gymapp/trainings/trainees-trainings").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainingBetweenDates))).andExpect(status().isCreated());
	}

	@Test
	void viewTrainerTrainingListPeriodTest() throws Exception {
		TrainingByDatesDTO trainingBetweenDates = new TrainingByDatesDTO("ramesh@gmail.com", new Date(), new Date());
		List<TrainerTrainingDTO> list = new ArrayList<>();
		list.add(TrainerTrainingDTO.builder().traineeUsername("acharya@gmail.com").trainingDate(new Date())
				.trainingName("test").build());
		Mockito.when(trainingService.displayTrainerTrainingsByDuration(trainingBetweenDates)).thenReturn(list);
		mockMvc.perform(post("/gymapp/trainings/trainers-trainings").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainingBetweenDates))).andExpect(status().isOk());
	}

	private static String asJsonString(Object object) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}