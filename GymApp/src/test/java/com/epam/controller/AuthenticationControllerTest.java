package com.epam.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.controller.AuthenticationController;
import com.epam.gymapp.model.User;
import com.epam.gymapp.service.Authentication;

@WebMvcTest(controllers = AuthenticationController.class)
class AuthenticationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private Authentication authentication;

	@Test
	void authenticateUserTest() throws Exception {
		User user = new User(0, "rama", "haresh", "999@gmail.com", "123", true);
		Mockito.when(authentication.userLogin("murari@gmail.com", "123")).thenReturn(user);
		mockMvc.perform(post("/gymapp/login").param("username", "murari@gmail.com").param("password", "123"))
				.andExpect(status().isOk());
	}

	@Test
	void changeLoginTest() throws Exception {
		User user = new User(0, "rama", "haresh", "999@gmail.com", "123", true);
		Mockito.when(authentication.changeLogin("murari@gmail.com", "123", "1234")).thenReturn(user);
		mockMvc.perform(post("/gymapp/login/update").param("username", "murari@gmail.com").param("oldPassword", "123")
				.param("newPassword", "1234")).andExpect(status().isOk());
	}
}