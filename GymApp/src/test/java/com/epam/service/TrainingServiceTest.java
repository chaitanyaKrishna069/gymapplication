package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gymapp.dto.input.TraineeDTO;
import com.epam.gymapp.dto.input.TrainingByDatesDTO;
import com.epam.gymapp.dto.input.TrainingDTO;
import com.epam.gymapp.dto.input.TrainingSummaryDto;
import com.epam.gymapp.dto.output.DisplayTraineeDTO;
import com.epam.gymapp.dto.output.TraineeTrainingDTO;
import com.epam.gymapp.dto.output.TrainerTrainingDTO;
import com.epam.gymapp.dto.output.TrainersDTO;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.Training;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.producer.ReportProducer;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.service.TraineeService;
import com.epam.gymapp.service.TrainerService;
import com.epam.gymapp.service.TrainingService;

@ExtendWith(MockitoExtension.class)
class TrainingServiceTest {
	@InjectMocks
	TrainingService trainingService;

	@Mock
	private TrainingRepository trainingRepository;

	@Mock
	private TraineeRepository traineeRepository;

	@Mock
	private TrainerRepository trainerRepository;

	@Mock
	private TrainingTypeRepository trainingTypeRepository;

	@Mock
	private TraineeService traineeService;

	@Mock
	private TrainerService trainerService;

	@Mock
	private ReportProducer reportProducer;

	User user;
	User user1;
	Trainee trainee;
	List<TrainersDTO> trainersDto;
	DisplayTraineeDTO viewTraineeDto;
	Trainer trainer1;
	TrainingType type;
	TraineeDTO traineeDto;
	List<Training> trainings;
	Training training;
	Trainer trainer;

	@BeforeEach
	public void setUp() {
		user = new User(0, "ramesh", "suresh", "prabhu", "123", true);
		user1 = new User(0, "rama", "sitha", "rajesh", "1223", true);
		trainee = new Trainee(0, new Date(), "vrindavan", user);
		trainer1 = new Trainer();
		trainer1.setUser(new User());
		type = new TrainingType(1, "Strength Training");
		trainer1.setSpecializationId(type);
		List<Trainer> trainers = new ArrayList<>();
		trainers.add(trainer1);
		trainee.setTrainers(trainers);

		trainer1.setTrainees(Collections.singletonList(trainee));
		trainings = new ArrayList<>();
		training = new Training(0, trainee, trainer1, "new", type, new Date(), 5);
		trainings.add(training);

		trainer = new Trainer(0, type, user1);
		List<Trainee> list = new ArrayList<>();
		list.add(trainee);
		trainer.setTrainees(list);
	}

	@Test
	void viewTraineeTrainingListWithPeriodTest() {
		TrainingByDatesDTO trainingBetweenDates = new TrainingByDatesDTO("prabhu", new Date(), new Date());
		Mockito.when(traineeService.getOriginalTrainee(trainingBetweenDates.getUsername())).thenReturn(trainee);
		Mockito.when(trainingRepository.findAllTrainingInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainee)).thenReturn(trainings);
		Mockito.when(trainerRepository.findById(anyInt())).thenReturn(Optional.ofNullable(trainer1));
		List<TraineeTrainingDTO> trainingsResult = trainingService
				.displayTraineeTrainingsByDuration(trainingBetweenDates);
		Mockito.verify(traineeService).getOriginalTrainee(trainingBetweenDates.getUsername());
		Mockito.verify(trainingRepository).findAllTrainingInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainee);
		Mockito.verify(trainerRepository).findById(anyInt());

	}

	@Test
	void viewTrainerTrainingListWithPeriodTest() {
		TrainingByDatesDTO trainingBetweenDates = new TrainingByDatesDTO("prabhu", new Date(), new Date());
		Mockito.when(trainerService.getOriginalTrainer(trainingBetweenDates.getUsername())).thenReturn(trainer);
		Mockito.when(trainingRepository.findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer)).thenReturn(trainings);
		Mockito.when(traineeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(trainee));
		List<TrainerTrainingDTO> trainingsResult = trainingService
				.displayTrainerTrainingsByDuration(trainingBetweenDates);
		Mockito.verify(trainerService).getOriginalTrainer(trainingBetweenDates.getUsername());
		Mockito.verify(trainingRepository).findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer);
		Mockito.verify(traineeRepository).findById(anyInt());

	}

	@Test
	void addTrainingTest() {
		TrainingDTO trainingDto = new TrainingDTO(trainee.getUser().getUserName(), trainer.getUser().getUserName(),
				"new", 1, new Date(), 5);
		Mockito.when(traineeService.getOriginalTrainee(anyString())).thenReturn(trainee);
		Mockito.when(trainerService.getOriginalTrainer(anyString())).thenReturn(trainer);
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		Mockito.when(trainerRepository.save(trainer)).thenReturn(trainer);
		Mockito.when(trainingTypeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(type));
		Mockito.when(trainingRepository.save(any(Training.class))).thenReturn(training);
		Mockito.doNothing().when(reportProducer).sendReports(any(TrainingSummaryDto.class));
		TrainingDTO result = trainingService.registerTraining(trainingDto);
		Mockito.verify(traineeService).getOriginalTrainee(anyString());
		Mockito.verify(trainerService).getOriginalTrainer(anyString());
		Mockito.verify(traineeRepository).save(trainee);
		Mockito.verify(trainerRepository).save(trainer);
		Mockito.verify(trainingTypeRepository).findById(anyInt());
		Mockito.verify(trainingRepository).save(any(Training.class));
		assertEquals(trainingDto, result);
	}
}