package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gymapp.dto.input.MailDTO;
import com.epam.gymapp.dto.input.TraineeDTO;
import com.epam.gymapp.dto.input.TraineeRegistrationDTO;
import com.epam.gymapp.dto.input.UpdatedTraineersDTO;
import com.epam.gymapp.dto.output.DisplayTraineeDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.TrainersDTO;
import com.epam.gymapp.dto.output.UpdatedTraineeDTO;
import com.epam.gymapp.exceptions.ApplicationException;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.producer.NotificationProducer;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.TraineeService;
import com.epam.gymapp.service.TrainerService;

@ExtendWith(MockitoExtension.class)
class TraineeServiceTest {

	@InjectMocks
	private TraineeService traineeService;

	@Mock
	private TraineeRepository traineeRepository;
	@Mock
	private TrainerRepository trainerRepository;
	@Mock
	private TrainerService trainerService;
	@Mock
	private UserRepository userRepository;
	@Mock
	private NotificationProducer notificationProducer;

	@Mock
	private PasswordEncoder encoder;

	User user;
	User user1;
	Trainee trainee;
	List<TrainersDTO> trainersDTOs;
	DisplayTraineeDTO displayTraineeDTO;
	Trainer trainer1;
	TrainingType type;
	TraineeDTO traineeDto;

	@BeforeEach
	public void setUp() {
		user = new User(0, "krishna", "gopala", "murari", "123", true);
		user1 = new User(0, "rama", "narayana", "padmanabha", "1223", true);
		trainee = new Trainee(0, new Date(), "vrindavan", user);
		trainer1 = new Trainer();
		trainer1.setUser(new User());
		type = new TrainingType(1, "Strength Training");
		trainer1.setSpecializationId(type);

		trainee.setTrainers(Collections.singletonList(trainer1));

		trainer1.setTrainees(Collections.singletonList(trainee));

		trainersDTOs = trainee.getTrainers().stream()
				.map(trainer -> TrainersDTO.builder().userName(trainer.getUser().getUserName())
						.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
						.specializationName(trainer.getSpecializationId().getTrainingTypeName()).build())
				.toList();
		displayTraineeDTO = DisplayTraineeDTO.builder().userName(user.getUserName()).firstName(user.getFirstName())
				.lastName(user.getLastName()).dateOfBirth(trainee.getDateOfBirth()).address(trainee.getAddress())
				.isActive(user.getIsActive()).trainers(trainersDTOs).build();
		traineeDto = new TraineeDTO(user.getUserName(), user.getFirstName(), user.getLastName(),
				trainee.getDateOfBirth(), trainee.getAddress(), user.getIsActive());
	}

	@Test
	void getTraineeByUsernameTest() {
		Optional<User> user = Optional.ofNullable(new User(0, "krishna", "gopala", "murari", "123", true));
		Optional<Trainee> trainee = Optional.ofNullable(new Trainee(0, new Date(), "vrindavan", user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(traineeRepository.findByUserId(user.get().getId())).thenReturn(trainee);
		Trainee requiredTrainee = traineeService.getOriginalTrainee("murari");
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.get().getId());
		assertEquals(trainee.get(), requiredTrainee);
	}
	


   

	@Test
	void getTraineeByUsernameExceptionTest() {
		Optional<User> user = Optional.ofNullable(new User(0, "krishna", "gopala", "murari", "123", true));
		Optional<Trainee> trainee = Optional.ofNullable(new Trainee(0, new Date(), "vrindavan", user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
		assertThrows(ApplicationException.class, () -> traineeService.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
	}

	@Test
	void getTraineeByUsernameException1Test() {
		Optional<User> user = Optional.ofNullable(new User(0, "krishna", "gopala", "murari", "123", true));
		Optional<Trainee> trainee = Optional.ofNullable(new Trainee(0, new Date(), "vrindavan", user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(traineeRepository.findByUserId(user.get().getId())).thenReturn(Optional.empty());
		assertThrows(ApplicationException.class, () -> traineeService.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.get().getId());
	}

	@Test
	void getTraineeDtoByUsernameTest() {
		String username = "murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));

		DisplayTraineeDTO required = traineeService.getTraineeByUsername(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		assertEquals(displayTraineeDTO, required);

	}

	@Test
	void getTraineeDtoByUsernameTest1() {
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
		assertThrows(ApplicationException.class, () -> traineeService.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");

	}

	@Test
	void getTraineeDtoByUsernameTest2() {
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.empty());
		assertThrows(ApplicationException.class, () -> traineeService.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());

	}

	@Test
	void updateTraineeTest1() {
		UpdatedTraineeDTO updatedTraineeDto = UpdatedTraineeDTO.builder().userName(user.getUserName())
				.firstName(user.getFirstName()).lastName(user.getLastName()).address(trainee.getAddress())
				.dateOfBirth(trainee.getDateOfBirth()).isActive(user.getIsActive()).build();

		updatedTraineeDto.setTrainersList(trainersDTOs);

		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);

		UpdatedTraineeDTO required = traineeService.updateTrainee(traineeDto);
		Mockito.verify(traineeRepository).save(trainee);
		assertEquals(updatedTraineeDto, required);

	}

	@Test
	void deleteTraineeTest() {
		String username = "murari";
		User user = new User(0, "krishna", "gopala", "murari", "123", true);
		Trainee trainee = new Trainee(0, new Date(), "vrindavan", user);
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		Mockito.doNothing().when(traineeRepository).delete(trainee);
		traineeService.deleteTrainee(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		Mockito.verify(traineeRepository).delete(trainee);

	}

	@Test
	void updateTraineers() {
		Trainee trainee = new Trainee();
		trainee.setUser(user1);
		trainee.getUser().setUserName("murari");

		List<Trainee> trainees = new ArrayList<>();
		trainees.add(trainee);
		Trainer trainer1 = new Trainer();

		trainer1.setUser(user1);
		trainer1.setSpecializationId(type);
		trainer1.setTrainees(trainees);

		Trainer trainer2 = new Trainer();
		trainer2.setUser(user);
		trainer2.setSpecializationId(new TrainingType(2, "Strength Training"));

		List<Trainer> list = new ArrayList<>();
		list.add(trainer1);
		list.add(trainer2);

		UpdatedTraineersDTO updateTraineeList = new UpdatedTraineersDTO();
		updateTraineeList.setUsername("murari");
		updateTraineeList.setTrainers(Arrays.asList("trainer1", "trainer2"));
		trainee.setTrainers(list);

		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));

		Mockito.when(trainerService.getOriginalTrainer("trainer1")).thenReturn(trainer1);
		Mockito.when(trainerService.getOriginalTrainer("trainer2")).thenReturn(trainer2);
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);

		List<TrainersDTO> result = traineeService.updateTrainers(updateTraineeList);

		Mockito.verify(traineeRepository).save(trainee);

		Mockito.verify(trainerService).getOriginalTrainer("trainer1");
		Mockito.verify(trainerService).getOriginalTrainer("trainer2");

		assertEquals(2, result.size());

	}

	@Test
	void notAssignedTrainersTest() {
		Trainee trainee = new Trainee();
		trainee.setUser(user1);
		trainee.getUser().setUserName("murari");

		List<Trainee> trainees = new ArrayList<>();
		trainees.add(trainee);

		Trainer trainer1 = new Trainer();

		trainer1.setUser(user1);
		trainer1.setSpecializationId(type);
		trainer1.setTrainees(trainees);

		Trainer trainer2 = new Trainer();
		trainer2.setUser(user);
		trainer2.setSpecializationId(new TrainingType(2, "Strength Training"));

		List<Trainer> list = new ArrayList<>();
		list.add(trainer1);
		list.add(trainer2);
		trainee.setTrainers(Arrays.asList(trainer1));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		Mockito.when(trainerRepository.findAll()).thenReturn(list);
		List<TrainersDTO> result = traineeService.unAssignedTrainers("murari");
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		Mockito.verify(trainerRepository).findAll();

		assertEquals(1, result.size());
	}

	@Test
	void addTraineeTest() {
		TraineeRegistrationDTO traineeRegDto = new TraineeRegistrationDTO("krishna", "gopala", "murari",
				trainee.getDateOfBirth(), trainee.getAddress());

		String password = traineeRegDto.getFirstName() + new Date().hashCode();
		User user = User.builder().firstName(traineeRegDto.getFirstName()).lastName(traineeRegDto.getLastName())
				.userName(traineeRegDto.getEmail()).password(encoder.encode(password)).isActive(true).build();
		Trainee trainee = Trainee.builder().address(traineeRegDto.getAddress())
				.dateOfBirth(traineeRegDto.getDateOfBirth()).user(user).build();
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		RegistrationResponseDTO responseDto = RegistrationResponseDTO.builder().username(traineeRegDto.getEmail())
				.password(password).build();
		Mockito.doNothing().when(notificationProducer).sendNotification(any(MailDTO.class));
		RegistrationResponseDTO result = traineeService.addTrainee(traineeRegDto);
		assertEquals(result.getUsername(), responseDto.getUsername());
	}
}