package com.epam.gymapp.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.input.MailDTO;

import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationProducer {

	@Autowired
	private KafkaTemplate<String, MailDTO> kafkaTemplate;

	public void sendNotification(MailDTO mailDTO) {
		log.info("email sent by kafka : " + mailDTO);
		kafkaTemplate.send("notifications", mailDTO);
	}

	@PreDestroy
	public void close() {
		if (kafkaTemplate != null) {
			log.info("closing kafka producer");
			kafkaTemplate.destroy();
		}
	}
}