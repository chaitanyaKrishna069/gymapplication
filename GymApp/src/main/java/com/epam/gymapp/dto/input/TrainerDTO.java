package com.epam.gymapp.dto.input;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerDTO {

	@NotBlank(message = "please enter valid username(email) !!")
	@Email(message = "please enter email!!")
	private String userName;
	@NotBlank(message = "please enter valid firstName !!")
	private String firstName;
	@NotBlank(message = "please enter valid lastName !!")
	private String lastName;

	private boolean isActive;
	@NotBlank(message = "please enter valid specializationName !!")
	private String specializationName;
}