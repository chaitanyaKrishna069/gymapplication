package com.epam.gymapp.dto.output;

import java.util.List;

import com.epam.gymapp.dto.TraineeListDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdatedTrainerDTO {

	private String userName;
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String specializationName;
	private List<TraineeListDto> traineesList;
}
