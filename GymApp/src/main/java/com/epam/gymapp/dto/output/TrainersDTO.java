package com.epam.gymapp.dto.output;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainersDTO {
	private String userName;
	private String firstName;
	private String lastName;
	
	private String specializationName;
}
