package com.epam.gymapp.dto.output;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TraineeTrainingDTO {

	private String trainingName;
	
	private int trainingTypeId;
	
	private Date trainingDate;
	
	private int trainingDuration;
	
	private String trainerUsername;
}
