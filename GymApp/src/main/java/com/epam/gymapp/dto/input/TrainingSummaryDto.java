package com.epam.gymapp.dto.input;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TrainingSummaryDto {

	private String trainingName;
	private Date trainingDate;
	private int trainingDuration;
	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
}