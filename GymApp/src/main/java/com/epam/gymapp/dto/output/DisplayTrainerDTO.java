package com.epam.gymapp.dto.output;

import java.util.List;

import com.epam.gymapp.dto.TraineeListDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DisplayTrainerDTO {
	
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String specializationName;
	private List<TraineeListDto> traineesList;
}
