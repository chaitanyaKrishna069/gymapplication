package com.epam.gymapp.dto.output;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdatedTraineeDTO {
	private String userName;
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private String address;
	private boolean isActive;
	private List<TrainersDTO> trainersList;
}