package com.epam.gymapp.dto.input;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TrainingDTO {

	private String traineeUserName;
	
	private String trainerUserName;
	
	private String trainingName;
	
	private int trainingTypeId;
	
	private Date trainingDate;
	
	private int trainingDuration;
}
