package com.epam.gymapp.dto.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MailDTO {

	private String toMail;
	private String ccMail;
	private String subject;
	private String body;
	
}