package com.epam.gymapp.exceptions;

@SuppressWarnings("serial")
public class ApplicationException extends RuntimeException {

	public ApplicationException() {
		super();
	}

	public ApplicationException(String message) {
		super(message);
	}

}