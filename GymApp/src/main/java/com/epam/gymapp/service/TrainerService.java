package com.epam.gymapp.service;

import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.TraineeListDto;
import com.epam.gymapp.dto.input.MailDTO;
import com.epam.gymapp.dto.input.TrainerDTO;
import com.epam.gymapp.dto.input.TrainerRegistrationDTO;
import com.epam.gymapp.dto.output.DisplayTrainerDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.UpdatedTrainerDTO;
import com.epam.gymapp.exceptions.ApplicationException;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.producer.NotificationProducer;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.repository.UserRepository;

@Service
public class TrainerService {

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private TrainingTypeRepository trainingTypeRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private NotificationProducer notificationProducer;

	@Autowired
	private PasswordEncoder encoder;

	public RegistrationResponseDTO addTrainer(TrainerRegistrationDTO trainerRegDto) {
		String password = trainerRegDto.getFirstName() + new Date().hashCode();
		User user = User.builder().firstName(trainerRegDto.getFirstName()).lastName(trainerRegDto.getLastName())
				.userName(trainerRegDto.getEmail()).password(encoder.encode(password)).isActive(true).build();
		TrainingType trainingType = trainingTypeRepository.findById(trainerRegDto.getSpecializationId())
				.orElseThrow(() -> new ApplicationException("enter valid training type id!!"));
		Trainer trainer = Trainer.builder().specializationId(trainingType).user(user).build();
		trainerRepository.save(trainer);
		String body = "welcome to gym application!! you are registered as trainer \n username: "
				+ trainerRegDto.getEmail() + "\n password: " + password + "\n thank you";
		String subject = "Registration Confirmation - GYM Application";
		MailDTO sendMailDto = new MailDTO(trainerRegDto.getEmail(), "jayk070707@gmail.com", subject, body);
		notificationProducer.sendNotification(sendMailDto);
		return RegistrationResponseDTO.builder().username(trainerRegDto.getEmail()).password(password).build();
	}

	public Trainer getOriginalTrainer(String username) {
		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new ApplicationException("enter valid username!!"));
		return trainerRepository.findByUserId(user.getId())
				.orElseThrow(() -> new ApplicationException("verify the user properly!!"));
	}

	public DisplayTrainerDTO getTrainerByUsername(String username) {
		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new ApplicationException("enter valid username!!"));
		Trainer trainer = trainerRepository.findByUserId(user.getId())
				.orElseThrow(() -> new ApplicationException("verify the user properly!!"));
		List<TraineeListDto> trainersDto = trainer.getTrainees().stream()
				.map(trainee -> TraineeListDto.builder().userName(trainee.getUser().getUserName())
						.firstName(trainee.getUser().getFirstName()).lastName(trainee.getUser().getLastName()).build())
				.toList();
		return DisplayTrainerDTO.builder().firstName(user.getFirstName()).lastName(user.getLastName())
				.isActive(user.getIsActive()).specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.traineesList(trainersDto).build();
	}

	public UpdatedTrainerDTO updateTrainer(TrainerDTO trainerDto) {
		Trainer trainer = getOriginalTrainer(trainerDto.getUserName());
		trainer.getUser().setFirstName(trainerDto.getFirstName());
		trainer.getUser().setLastName(trainerDto.getLastName());
		trainer.getUser().setIsActive(trainerDto.isActive());
		trainerRepository.save(trainer);
		UpdatedTrainerDTO updatedTrainerDto = UpdatedTrainerDTO.builder().userName(trainerDto.getUserName())
				.firstName(trainerDto.getFirstName()).lastName(trainerDto.getLastName())
				.specializationName(trainerDto.getSpecializationName()).isActive(trainerDto.isActive()).build();
		updatedTrainerDto.setSpecializationName(trainerDto.getSpecializationName());
		updatedTrainerDto.setTraineesList(trainer.getTrainees().stream()
				.map(trainee -> TraineeListDto.builder().userName(trainee.getUser().getUserName())
						.firstName(trainee.getUser().getFirstName()).lastName(trainee.getUser().getLastName()).build())
				.toList());
		notificationProducer.sendNotification(MailDTO.builder().toMail(updatedTrainerDto.getUserName())
				.ccMail("069cck@gmail.com").body("your profile is updated!!").subject("Update alert!!!").build());
		return updatedTrainerDto;
	}
}