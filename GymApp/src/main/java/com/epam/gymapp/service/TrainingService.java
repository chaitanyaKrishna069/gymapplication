package com.epam.gymapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.input.TrainingByDatesDTO;
import com.epam.gymapp.dto.input.TrainingDTO;
import com.epam.gymapp.dto.input.TrainingSummaryDto;
import com.epam.gymapp.dto.output.TraineeTrainingDTO;
import com.epam.gymapp.dto.output.TrainerTrainingDTO;
import com.epam.gymapp.exceptions.ApplicationException;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.Training;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.producer.ReportProducer;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;

@Service
public class TrainingService {

	@Autowired
	private TrainingRepository trainingRepository;

	@Autowired
	private TraineeRepository traineeRepository;

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private TrainingTypeRepository trainingTypeRepository;

	@Autowired
	private TraineeService traineeService;

	@Autowired
	private TrainerService trainerService;

	@Autowired
	private ReportProducer reportProducer;

	public TrainingDTO registerTraining(TrainingDTO trainingDTO) {
		Trainee trainee = traineeService.getOriginalTrainee(trainingDTO.getTraineeUserName());
		Trainer trainer = trainerService.getOriginalTrainer(trainingDTO.getTrainerUserName());
		trainee.getTrainers().add(trainer);
		trainer.getTrainees().add(trainee);
		traineeRepository.save(trainee);
		trainerRepository.save(trainer);
		TrainingType trainingType = trainingTypeRepository.findById(trainingDTO.getTrainingTypeId())
				.orElseThrow(() -> new ApplicationException("enter valid trainingType"));
		Training training = Training.builder().trainee(trainee).trainer(trainer)
				.trainingName(trainingDTO.getTrainingName()).trainingType(trainingType)
				.trainingDate(trainingDTO.getTrainingDate()).trainingDuration(trainingDTO.getTrainingDuration())
				.build();
		trainingRepository.save(training);
		reportProducer.sendReports(TrainingSummaryDto.builder().trainingName(trainingDTO.getTrainingName())
				.trainingDate(trainingDTO.getTrainingDate()).trainingDuration(trainingDTO.getTrainingDuration())
				.trainerUsername(trainer.getUser().getUserName()).trainerFirstName(trainer.getUser().getFirstName())
				.trainerLastName(trainer.getUser().getLastName()).build());
		return trainingDTO;
	}

	public List<TraineeTrainingDTO> displayTraineeTrainingsByDuration(TrainingByDatesDTO trainingByDatesDTO) {
		Trainee trainee = traineeService.getOriginalTrainee(trainingByDatesDTO.getUsername());
		List<Training> list = trainingRepository.findAllTrainingInBetween(trainingByDatesDTO.getFrom(),
				trainingByDatesDTO.getTo(), trainee);
		return list.stream()
				.map(training -> TraineeTrainingDTO.builder().trainingName(training.getTrainingName())
						.trainingTypeId(training.getTrainingType().getId()).trainingDate(training.getTrainingDate())
						.trainingDuration(training.getTrainingDuration()).trainerUsername(trainerRepository
								.findById(training.getTrainee().getId()).get().getUser().getFirstName())
						.build())
				.toList();
	}

	public List<TrainerTrainingDTO> displayTrainerTrainingsByDuration(TrainingByDatesDTO trainingBetweenDates) {
		Trainer trainer = trainerService.getOriginalTrainer(trainingBetweenDates.getUsername());
		List<Training> list = trainingRepository.findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer);
		return list.stream().map(training -> TrainerTrainingDTO.builder()
				.traineeUsername(
						traineeRepository.findById(training.getTrainee().getId()).get().getUser().getFirstName())
				.trainingName(training.getTrainingName()).trainingDate(training.getTrainingDate())
				.trainingDuration(training.getTrainingDuration()).build()).toList();
	}
}