package com.epam.gymapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.input.MailDTO;
import com.epam.gymapp.dto.input.TraineeDTO;
import com.epam.gymapp.dto.input.TraineeRegistrationDTO;
import com.epam.gymapp.dto.input.UpdatedTraineersDTO;
import com.epam.gymapp.dto.output.DisplayTraineeDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.TrainersDTO;
import com.epam.gymapp.dto.output.UpdatedTraineeDTO;
import com.epam.gymapp.exceptions.ApplicationException;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.User;
import com.epam.gymapp.producer.NotificationProducer;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TraineeService {

	@Autowired
	private TraineeRepository traineeRepository;

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TrainerService trainerService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private NotificationProducer notificationProducer;

	public RegistrationResponseDTO addTrainee(TraineeRegistrationDTO traineeRegistrationDTO) {
		log.info("Adding trainee: {}", traineeRegistrationDTO.getEmail());
		String password = traineeRegistrationDTO.getFirstName() + "123";
		User user = User.builder().firstName(traineeRegistrationDTO.getFirstName())
				.lastName(traineeRegistrationDTO.getLastName()).userName(traineeRegistrationDTO.getEmail())
				.password(passwordEncoder.encode(password)).isActive(true).build();
		Trainee trainee = Trainee.builder().address(traineeRegistrationDTO.getAddress())
				.dateOfBirth(traineeRegistrationDTO.getDateOfBirth()).user(user).build();
		traineeRepository.save(trainee);
		RegistrationResponseDTO responseDto = RegistrationResponseDTO.builder()
				.username(traineeRegistrationDTO.getEmail()).password(password).build();
		String body = "welcome to Gym, you are registered as trainee \n username: " + responseDto.getUsername()
				+ "\n password: " + responseDto.getPassword() + "\n thank you";
		String subject = "Successfully Completed Registration";
		MailDTO sendMailDto = new MailDTO(responseDto.getUsername(), "069cck@gmail.com", subject, body);

		notificationProducer.sendNotification(sendMailDto);
		log.info("Trainee added successfully: {}", traineeRegistrationDTO.getEmail());
		log.info("Notification sent for trainee: {}", traineeRegistrationDTO.getEmail());
		return responseDto;
	}

	public Trainee getOriginalTrainee(String username) {
		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new ApplicationException("enter valid username"));
		return traineeRepository.findByUserId(user.getId())
				.orElseThrow(() -> new ApplicationException("verify the user properly"));
	}

	public DisplayTraineeDTO getTraineeByUsername(String username) {
		log.info("Getting trainee by username: {}", username);
		User user = userRepository.findByUserName(username).orElseThrow(() -> {
			log.error("Username '{}' not found in the repository", username);
			return new ApplicationException("Enter valid username");
		});
		log.info("User found with username: {}", username);

		Trainee trainee = traineeRepository.findByUserId(user.getId()).orElseThrow(() -> {
			log.error("Trainee not found for user with ID: {}", user.getId());
			return new ApplicationException("Verify the user properly");
		});

		log.info("Trainee found for user with ID: {}", user.getId());
		List<TrainersDTO> trainersDto = trainee.getTrainers().stream()
				.map(trainer -> TrainersDTO.builder().userName(trainer.getUser().getUserName())
						.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
						.specializationName(trainer.getSpecializationId().getTrainingTypeName()).build())
				.toList();
		log.info("Mapped trainers DTO for trainee with username: {}", username);

		DisplayTraineeDTO displayTraineeDTO = DisplayTraineeDTO.builder().userName(username)
				.firstName(user.getFirstName()).lastName(user.getLastName()).dateOfBirth(trainee.getDateOfBirth())
				.address(trainee.getAddress()).isActive(user.getIsActive()).trainers(trainersDto).build();

		log.info("Built DisplayTraineeDTO for trainee with username: {}", username);

		return displayTraineeDTO;
	}

	public UpdatedTraineeDTO updateTrainee(TraineeDTO traineeDto) {
		log.info("Updating trainee: {}", traineeDto.getUserName());

		Trainee trainee = getOriginalTrainee(traineeDto.getUserName());
		trainee.setAddress(traineeDto.getAddress());
		trainee.setDateOfBirth(traineeDto.getDateOfBirth());
		trainee.getUser().setFirstName(traineeDto.getFirstName());
		trainee.getUser().setLastName(traineeDto.getLastName());
		trainee.getUser().setIsActive(traineeDto.isActive());
		traineeRepository.save(trainee);

		UpdatedTraineeDTO updatedTraineeDto = UpdatedTraineeDTO.builder().userName(traineeDto.getUserName())
				.firstName(traineeDto.getFirstName()).lastName(traineeDto.getLastName())
				.address(traineeDto.getAddress()).dateOfBirth(traineeDto.getDateOfBirth())
				.isActive(traineeDto.isActive()).build();

		updatedTraineeDto.setTrainersList(trainee.getTrainers().stream()
				.map(trainer -> TrainersDTO.builder().userName(trainer.getUser().getUserName())
						.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
						.specializationName(trainer.getSpecializationId().getTrainingTypeName()).build())
				.toList());

		MailDTO mailDto = MailDTO.builder().toMail(updatedTraineeDto.getUserName()).ccMail("069cck@gmail.com")
				.body("Your profile is updated").subject("Update alert").build();
		notificationProducer.sendNotification(mailDto);

		log.info("Notification sent to trainee: {}", traineeDto.getUserName());
		log.info("Trainee updated: {}", traineeDto.getUserName());
		return updatedTraineeDto;
	}

	public void deleteTrainee(String username) {
		log.info("Deleting trainee with username: {}", username);
		try {
			Trainee trainee = getOriginalTrainee(username);
			traineeRepository.delete(trainee);

			log.info("Trainee deleted successfully. Username: {}", username);
		} catch (Exception exception) {
			log.error("Error deleting trainee: {}", exception.getMessage(), exception);
			throw new ApplicationException("Error deleting trainee");
		}
	}

	public List<TrainersDTO> updateTrainers(UpdatedTraineersDTO updatedTraineersDTO) {
		log.info("Updating trainers for trainee: {}", updatedTraineersDTO.getUsername());

		Trainee trainee = getOriginalTrainee(updatedTraineersDTO.getUsername());
		log.debug("Original trainee: {}", trainee);

		List<Trainer> trainers = updatedTraineersDTO.getTrainers().stream().map(name -> {
			Trainer trainer = trainerService.getOriginalTrainer(name);
			log.debug("Original trainer for name {}: {}", name, trainer);
			return trainer;
		}).toList();

		log.debug("Clearing existing trainers for trainee: {}", trainee.getUser());
		trainee.getTrainers().clear();

		log.debug("Adding updated trainers for trainee: {}", trainee.getUser());
		trainee.getTrainers().addAll(trainers);

		log.debug("Saving updated trainee: {}", trainee);
		traineeRepository.save(trainee);
		List<TrainersDTO> trainersDTOs = trainers.stream()
				.map(trainer -> TrainersDTO.builder().userName(trainer.getUser().getUserName())
						.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
						.specializationName(trainer.getSpecializationId().getTrainingTypeName()).build())
				.toList();

		log.info("Updated trainers for trainee {}: {}", updatedTraineersDTO.getUsername(), trainersDTOs);
		return trainersDTOs;
	}

	public List<TrainersDTO> unAssignedTrainers(String username) {
		log.info("Fetching unassigned trainers for trainee: {}", username);

		Trainee trainee = getOriginalTrainee(username);
		log.debug("Trainee fetched: {}", trainee);

		List<Trainer> assignedTrainers = trainee.getTrainers();
		log.debug("Assigned trainers: {}", assignedTrainers);

		List<Trainer> allTrainers = trainerRepository.findAll();
		log.debug("All trainers fetched: {}", allTrainers);

		allTrainers.removeAll(assignedTrainers);

		log.info("Returning unassigned trainers count: {}", allTrainers.size());

		return allTrainers.stream()
				.map(trainer -> TrainersDTO.builder().userName(trainer.getUser().getUserName())
						.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
						.specializationName(trainer.getSpecializationId().getTrainingTypeName()).build())
				.toList();
	}
}