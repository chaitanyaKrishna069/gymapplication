package com.epam.gymapp.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.exceptions.ApplicationException;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Authentication {

	@Autowired
	private UserRepository userRepository;

	public User userLogin(String username, String password) {
		log.info("Attempting login for username: {}", username);
		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(password))) {
			log.warn("Login failed for username: {}", username);
			throw new ApplicationException("Enter Proper Credentials");
		}
		log.info("Login successful for username: {}", username);
		return user.get();
	}

	public User changeLogin(String username, String oldPassword, String newPassword) {
		log.info("Changing login credentials for user: {}", username);
		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(oldPassword))) {
			log.warn("Failed login credentials change attempt for user: {}", username);
			throw new ApplicationException("Enter Proper Credentials");
		}
		log.debug("Updating password for user: {}", username);
		user.get().setPassword(newPassword);
		userRepository.save(user.get());
		log.info("Login credentials changed successfully for user: {}", username);
		return user.get();
	}
}