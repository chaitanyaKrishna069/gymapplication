package com.epam.gymapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.input.TrainingByDatesDTO;
import com.epam.gymapp.dto.input.TrainingDTO;
import com.epam.gymapp.dto.output.TraineeTrainingDTO;
import com.epam.gymapp.dto.output.TrainerTrainingDTO;
import com.epam.gymapp.service.TrainingService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/gymapp/trainings")
public class TrainingController {

	@Autowired
	private TrainingService trainingService;

	@PostMapping
	public ResponseEntity<TrainingDTO> addTraining(@RequestBody @Valid TrainingDTO trainingDTO) {
		return new ResponseEntity<>(trainingService.registerTraining(trainingDTO), HttpStatus.CREATED);
	}

	@PostMapping("/trainees-trainings")
	public ResponseEntity<List<TraineeTrainingDTO>> displayTraineesTrainings(
			@RequestBody @Valid TrainingByDatesDTO trainingBetweenDates) {
		return new ResponseEntity<>(trainingService.displayTraineeTrainingsByDuration(trainingBetweenDates),
				HttpStatus.CREATED);
	}

	@PostMapping("/trainers-trainings")
	public ResponseEntity<List<TrainerTrainingDTO>> displayTrainerTrainings(
			@RequestBody @Valid TrainingByDatesDTO trainingBetweenDates) {
		return new ResponseEntity<>(trainingService.displayTrainerTrainingsByDuration(trainingBetweenDates),
				HttpStatus.OK);
	}
}