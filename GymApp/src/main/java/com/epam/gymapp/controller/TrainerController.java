package com.epam.gymapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.input.TrainerDTO;
import com.epam.gymapp.dto.input.TrainerRegistrationDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.UpdatedTrainerDTO;
import com.epam.gymapp.dto.output.DisplayTrainerDTO;
import com.epam.gymapp.service.TrainerService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/gymapp/trainers")
public class TrainerController {

	@Autowired
	private TrainerService trainerService;

	@PostMapping("/register")
	public ResponseEntity<RegistrationResponseDTO> addTrainee(
			@RequestBody @Valid TrainerRegistrationDTO trainerRegistrationDTO) {
		return new ResponseEntity<>(trainerService.addTrainer(trainerRegistrationDTO), HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<UpdatedTrainerDTO> updateTrainer(@RequestBody @Valid TrainerDTO trainerDto) {
		return new ResponseEntity<>(trainerService.updateTrainer(trainerDto), HttpStatus.CREATED);
	}

	@GetMapping("/{username}")
	public ResponseEntity<DisplayTrainerDTO> displayTrainer(@PathVariable String username) {
		return new ResponseEntity<>(trainerService.getTrainerByUsername(username), HttpStatus.OK);
	}
}