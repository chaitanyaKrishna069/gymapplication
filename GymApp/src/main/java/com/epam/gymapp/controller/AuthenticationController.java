package com.epam.gymapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.model.User;
import com.epam.gymapp.service.Authentication;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;

@RestController
@RequestMapping("/gymapp/login")
public class AuthenticationController {

	@Autowired
	private Authentication authentication;

	@PostMapping
	public ResponseEntity<User> authenticateUser(
			@RequestParam @Valid @NotBlank(message = "username must not be blank!!") String username,
			@RequestParam @Valid @NotBlank(message = "password must not be blank!!") String password) {
		return new ResponseEntity<>(authentication.userLogin(username, password), HttpStatus.OK);
	}

	@PostMapping("/update")
	public ResponseEntity<User> updateLoginCredentials(
			@RequestParam @Valid @NotBlank(message = "username must not be blank!!") String username,
			@RequestParam @Valid @NotBlank(message = "oldPassword must not be blank!!") String oldPassword,
			@RequestParam @Valid @NotBlank(message = "newPassword must not be blank!!") String newPassword) {
		return new ResponseEntity<>(authentication.changeLogin(username, oldPassword, newPassword), HttpStatus.OK);
	}
}