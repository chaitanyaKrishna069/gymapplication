package com.epam.gymapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.input.TraineeDTO;
import com.epam.gymapp.dto.input.TraineeRegistrationDTO;
import com.epam.gymapp.dto.input.UpdatedTraineersDTO;
import com.epam.gymapp.dto.output.RegistrationResponseDTO;
import com.epam.gymapp.dto.output.TrainersDTO;
import com.epam.gymapp.dto.output.UpdatedTraineeDTO;
import com.epam.gymapp.dto.output.DisplayTraineeDTO;
import com.epam.gymapp.service.TraineeService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/gymapp/trainees")
public class TraineeController {

	@Autowired
	private TraineeService traineeService;

	@PostMapping("/register")
	public ResponseEntity<RegistrationResponseDTO> addTrainee(
			@RequestBody @Valid TraineeRegistrationDTO traineeRegistration) {
		return new ResponseEntity<>(traineeService.addTrainee(traineeRegistration), HttpStatus.CREATED);
	}

	@GetMapping("/{username}")
	public ResponseEntity<DisplayTraineeDTO> viewTrainee(@PathVariable String username) {
		return new ResponseEntity<>(traineeService.getTraineeByUsername(username), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<UpdatedTraineeDTO> updateTrainee(@RequestBody @Valid TraineeDTO traineeDto) {
		return new ResponseEntity<>(traineeService.updateTrainee(traineeDto), HttpStatus.CREATED);
	}

	@DeleteMapping("/{username}")
	public ResponseEntity<Void> deleteTrainee(@PathVariable String username) {
		traineeService.deleteTrainee(username);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping("/trainers-updates")
	public ResponseEntity<List<TrainersDTO>> updateTraineers(@RequestBody @Valid UpdatedTraineersDTO updatedTraineers) {
		return new ResponseEntity<>(traineeService.updateTrainers(updatedTraineers), HttpStatus.CREATED);
	}

	@GetMapping("/trainers-un-assigned/{username}")
	public ResponseEntity<List<TrainersDTO>> unAssignedTraineers(@PathVariable String username) {
		return new ResponseEntity<>(traineeService.unAssignedTrainers(username), HttpStatus.ACCEPTED);
	}
}