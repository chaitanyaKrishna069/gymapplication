package com.epam.gateway.filter;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Predicate;

@Component
public class RouteFilter {

	public static final List<String> openApiEndpoints = List.of("/eureka", "/gymapp/trainees/register",
			"/gymapp/trainers/register", "/auth/token", "/auth/validate");

	public final Predicate<ServerHttpRequest> isSecured = request -> openApiEndpoints.stream()
			.noneMatch(uri -> request.getURI().getPath().contains(uri));
}