package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.gymapp.input.dto.MailDTO;
import com.epam.notifications.model.Mail;
import com.epam.notifications.repository.MailRepository;
import com.epam.notifications.service.MailService;

@ExtendWith(MockitoExtension.class)
class MailServiceTest {

	@Mock
	private JavaMailSender mockMailSender;

	@Mock
	private MailRepository mockMailRepository;

	@InjectMocks
	private MailService mailService;

	@BeforeEach
	void setUp() {
		mockMailSender = mock(JavaMailSender.class);
		mockMailRepository = mock(MailRepository.class);
		mailService = new MailService(mockMailSender, mockMailRepository);
	}

	@Test
	void testSendEmail_Success() {
		MailDTO sendMailDto = new MailDTO();
		sendMailDto.setToMail("recipient@example.com");
		sendMailDto.setSubject("Test Subject");
		sendMailDto.setBody("Test Body");
		sendMailDto.setCcMail("cc@example.com");

		SimpleMailMessage expectedMessage = new SimpleMailMessage();
		expectedMessage.setTo(sendMailDto.getToMail());
		expectedMessage.setSubject(sendMailDto.getSubject());
		expectedMessage.setText(sendMailDto.getBody());
		expectedMessage.setCc(sendMailDto.getCcMail());

		doNothing().when(mockMailSender).send(expectedMessage);
		mailService.sendEmail(sendMailDto);
		verify(mockMailSender, times(1)).send(expectedMessage);
	}

	@Test
	void testSendEmail_Failure() {
		MailDTO sendMailDto = new MailDTO();
		sendMailDto.setToMail("recipient@example.com");
		sendMailDto.setSubject("Test Subject");
		sendMailDto.setBody("Test Body");
		sendMailDto.setCcMail("cc@example.com");

		SimpleMailMessage expectedMessage = new SimpleMailMessage();
		expectedMessage.setTo(sendMailDto.getToMail());
		expectedMessage.setSubject(sendMailDto.getSubject());
		expectedMessage.setText(sendMailDto.getBody());
		expectedMessage.setCc(sendMailDto.getCcMail());

		doThrow(new MailSendException("Failed to send email")).when(mockMailSender).send(any(SimpleMailMessage.class));

		mailService.sendEmail(sendMailDto);
		ArgumentCaptor<Mail> mailArgumentCaptor = ArgumentCaptor.forClass(Mail.class);
		verify(mockMailRepository, times(1)).save(mailArgumentCaptor.capture());

		Mail savedMail = mailArgumentCaptor.getValue();
		assertEquals("failed", savedMail.getStatus());
		assertNotNull(savedMail.getRemarks());
	}
}