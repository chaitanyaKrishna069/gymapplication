package com.epam.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.gymapp.input.dto.MailDTO;
import com.epam.notifications.controller.MailController;
import com.epam.notifications.service.MailService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = MailController.class)
class MailControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MailService mailService;

	@Test
	void testSendMailDto_ValidDto_ReturnsCreated() throws Exception {
		MailDTO validDto = new MailDTO("069cck@gmail.com", "recipient@example.com", "Subject", "Body");

		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(validDto);
		mockMvc.perform(MockMvcRequestBuilders.post("/mails").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(MockMvcResultMatchers.status().isCreated());

		verify(mailService, times(1)).sendEmail(validDto);
	}

	@Test
	void testSendMailDto_NullDto_ReturnsBadRequest() throws Exception {
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(null);
		mockMvc.perform(MockMvcRequestBuilders.post("/mails").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());

		verify(mailService, never()).sendEmail(any());
	}
}