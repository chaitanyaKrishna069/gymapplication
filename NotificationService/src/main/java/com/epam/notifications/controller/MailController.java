package com.epam.notifications.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.input.dto.MailDTO;
import com.epam.notifications.service.MailService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/mails")
public class MailController {

	@Autowired
	private MailService mailService;

	@PostMapping
	public ResponseEntity<Void> sendMailDto(@RequestBody @Valid MailDTO sendMailDto) {
		mailService.sendEmail(sendMailDto);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}