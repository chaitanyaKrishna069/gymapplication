package com.epam.notifications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.gymapp.input.dto.MailDTO;
import com.epam.notifications.model.Mail;
import com.epam.notifications.repository.MailRepository;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class MailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private MailRepository mailRepository;

	public void sendEmail(MailDTO sendMailDto) {

		log.info("Sending email to: {}", sendMailDto.getToMail());
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(sendMailDto.getToMail());
		message.setSubject(sendMailDto.getSubject());
		message.setText(sendMailDto.getBody());
		message.setCc(sendMailDto.getCcMail());
		Mail mail = new Mail(1, "chaitanyakrishnach2000@gmail.com", sendMailDto.getToMail(), sendMailDto.getCcMail(),
				sendMailDto.getBody(), "success", "remarks");
		try {
			javaMailSender.send(message);
			log.info("Email sent successfully.");
		} catch (Exception exception) {
			mail.setStatus("failed");
			mail.setRemarks(exception.getMessage());
			log.error("Failed to send email: {}", exception.getMessage(), exception);
		}
		mailRepository.save(mail);
		log.info("Email record saved to database.");
	}
}