package com.epam.notifications.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "service")
public class Mail {
	@Id
	int id;
	private String fromEmail;
	private String toEmail;
	private String ccEmail;
	private String body;
	private String status;
	private String remarks;
}