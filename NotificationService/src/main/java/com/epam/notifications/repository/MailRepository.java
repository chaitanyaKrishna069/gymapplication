package com.epam.notifications.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.epam.notifications.model.Mail;

@Repository
public interface MailRepository extends MongoRepository<Mail, String> {

}
