package com.epam.notifications.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.gymapp.input.dto.MailDTO;
import com.epam.notifications.service.MailService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationConsumer {

	@Autowired
	MailService mailService;

	@KafkaListener(topics = "notifications", groupId = "myGroup")
	public void consumer(MailDTO sendMailDto) {
		log.info("message received : {} " ,sendMailDto);
		mailService.sendEmail(sendMailDto);
	}
}