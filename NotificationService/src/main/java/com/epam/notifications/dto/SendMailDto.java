package com.epam.notifications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SendMailDto {

	private String toMail;
	private String ccMail;
	private String subject;
	private String body;
}