package com.epam.report.service.exception;

@SuppressWarnings("serial")
public class ReportsException extends RuntimeException {

	public ReportsException() {
		super();
	}

	public ReportsException(String message) {
		super(message);
	}
}