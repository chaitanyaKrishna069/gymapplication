package com.epam.report.service.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(ReportsException.class)
	@ResponseStatus(value = HttpStatus.OK)
	public ExceptionResponse exceptionHandler(ReportsException exception, WebRequest request) {
		log.info("UserException occured!!!");
		return new ExceptionResponse(new Date().toString(), HttpStatus.OK.toString(), exception.getMessage(),
				request.getContextPath());
	}
}