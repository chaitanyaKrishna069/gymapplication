package com.epam.report.service.model;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Timings")
public class TrainingSummary {

	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
	private String status;
	private Map<Integer, Map<Integer, Map<Integer, Integer>>> yearSummary;
}