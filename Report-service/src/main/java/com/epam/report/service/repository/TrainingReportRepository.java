package com.epam.report.service.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.epam.report.service.model.TrainingSummary;

@Repository
public interface TrainingReportRepository extends MongoRepository<TrainingSummary, String> {

}
