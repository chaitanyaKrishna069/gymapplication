package com.epam.report.service.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.input.TrainingSummaryDto;
import com.epam.report.service.service.ReportService;

@Service
public class ReportConsumer {

	@Autowired
	private ReportService reportService;

	@KafkaListener(topics = "reports", groupId = "myGroup")
	public void consumer(TrainingSummaryDto trainingSummaryDto) {
		reportService.maintaineReport(trainingSummaryDto);
	}
}