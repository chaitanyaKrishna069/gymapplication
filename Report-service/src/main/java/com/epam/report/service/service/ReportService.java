package com.epam.report.service.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.input.TrainingSummaryDto;
import com.epam.report.service.dto.TrainingSummaryResponse;
import com.epam.report.service.exception.ReportsException;
import com.epam.report.service.model.TrainingSummary;
import com.epam.report.service.repository.TrainingReportRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReportService {

	@Autowired
	private TrainingReportRepository trainingReportRepository;

	public void maintaineReport(TrainingSummaryDto trainingSummaryDto) {
		log.info("Maintaining Trainer Report with details {}", trainingSummaryDto.toString());
		TrainingSummary trainingSummary = trainingReportRepository.findById(trainingSummaryDto.getTrainerUsername())
				.orElse(TrainingSummary.builder().trainerUsername(trainingSummaryDto.getTrainerUsername())
						.trainerFirstName(trainingSummaryDto.getTrainerFirstName())
						.trainerLastName(trainingSummaryDto.getTrainerLastName()).yearSummary(new HashMap<>()).build());
		Map<Integer, Map<Integer, Map<Integer, Integer>>> newResult = trainingSummary.getYearSummary();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(trainingSummaryDto.getTrainingDate());

		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		Map<Integer, Map<Integer, Integer>> yearReport = newResult.getOrDefault(year, new HashMap<>());
		Map<Integer, Integer> monthReport = yearReport.getOrDefault(month, new HashMap<>());
		monthReport.put(day, monthReport.getOrDefault(day, 0) + trainingSummaryDto.getTrainingDuration());
		yearReport.put(month, monthReport);
		newResult.put(year, yearReport);

		trainingSummary.setYearSummary(newResult);
		trainingReportRepository.save(trainingSummary);
		log.info("Submited the Trainer Report with details {}", trainingSummaryDto.toString());
	}

	public TrainingSummaryResponse displayReport(String username) {
		log.info("Displaying Trainer Report for username: {}", username);
		TrainingSummary trainingSummary = trainingReportRepository.findById(username)
				.orElseThrow(() -> new ReportsException("no username found!!"));
		return TrainingSummaryResponse.builder().trainerFirstName(trainingSummary.getTrainerFirstName())
				.trainerLastName(trainingSummary.getTrainerLastName())
				.trainerUsername(trainingSummary.getTrainerUsername()).yearSummary(trainingSummary.getYearSummary())
				.status(trainingSummary.getStatus()).build();
	}
}