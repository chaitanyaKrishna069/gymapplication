package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gymapp.dto.input.TrainingSummaryDto;
import com.epam.report.service.dto.TrainingSummaryResponse;
import com.epam.report.service.exception.ReportsException;
import com.epam.report.service.model.TrainingSummary;
import com.epam.report.service.repository.TrainingReportRepository;
import com.epam.report.service.service.ReportService;

@ExtendWith(MockitoExtension.class)
class ReportServiceTest {

	@Mock
	private TrainingReportRepository trainingReportRepository;

	@InjectMocks
	private ReportService reportService;

	@Test
	void testMaintaineReport_NewTrainerYearMonthDay() {

		TrainingSummaryDto dto = new TrainingSummaryDto("Training", new Date(), 60, "trainer123", "John", "Doe");
		when(trainingReportRepository.findById(dto.getTrainerUsername())).thenReturn(Optional.empty());
		reportService.maintaineReport(dto);
		verify(trainingReportRepository, times(1)).findById(dto.getTrainerUsername());
		verify(trainingReportRepository, times(1)).save(any(TrainingSummary.class));
	}

	@Test
	void testMaintaineReport_ExistingTrainerYearMonthDay() {

		TrainingSummaryDto dto = new TrainingSummaryDto("Training", new Date(), 60, "trainer123", "John", "Doe");
		TrainingSummary existingSummary = TrainingSummary.builder().trainerUsername(dto.getTrainerUsername())
				.yearSummary(new HashMap<>()).build();
		when(trainingReportRepository.findById(dto.getTrainerUsername())).thenReturn(Optional.of(existingSummary));
		reportService.maintaineReport(dto);
		verify(trainingReportRepository, times(1)).findById(dto.getTrainerUsername());
		verify(trainingReportRepository, times(1)).save(any(TrainingSummary.class));
	}

	@Test
	void testMaintaineReport_ErrorInRepository() {

		TrainingSummaryDto dto = new TrainingSummaryDto("Training", new Date(), 60, "trainer123", "John", "Doe");
		when(trainingReportRepository.findById(dto.getTrainerUsername()))
				.thenThrow(new RuntimeException("Repository error"));
		assertThrows(RuntimeException.class, () -> reportService.maintaineReport(dto));
		verify(trainingReportRepository, times(1)).findById(dto.getTrainerUsername());
		verify(trainingReportRepository, never()).save(any(TrainingSummary.class));
	}

	@Test
	void testDiplayReport_ExistingUsername() {

		TrainingSummary existingSummary = TrainingSummary.builder().trainerUsername("trainer123")
				.trainerFirstName("John").trainerLastName("Doe").yearSummary(new HashMap<>()).status("success").build();
		when(trainingReportRepository.findById("trainer123")).thenReturn(Optional.of(existingSummary));
		TrainingSummaryResponse response = reportService.displayReport("trainer123");
		verify(trainingReportRepository, times(1)).findById("trainer123");
		assertEquals("John", response.getTrainerFirstName());
		assertEquals("Doe", response.getTrainerLastName());
		assertEquals("trainer123", response.getTrainerUsername());
		assertEquals(existingSummary.getYearSummary(), response.getYearSummary());
		assertEquals("success", response.getStatus());
	}

	@Test
    void testDiplayReport_NonExistingUsername() {
        when(trainingReportRepository.findById("nonexistent")).thenReturn(Optional.empty());
        assertThrows(ReportsException.class, () -> reportService.displayReport("nonexistent"));
        verify(trainingReportRepository, times(1)).findById("nonexistent");
    }
}